<?php

namespace controllers;

use vendor\components\AbstractController;
use vendor\components\Validator;

class HomeController extends AbstractController
{
    /**
     * @return array
     */
    public function index()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        foreach ($data["filters"] as $filter) {
            $filterName = 'filters\\' . $filter["filter"];
            if(!class_exists($filterName)) {
                return ['data' => ["errors" => "Unknown filter"], 'code' => 422];
            }
            $filterObject = new $filterName($data["image"], $filter["options"]);
            $validator = new Validator($filter["options"], $filterObject->getRules());
            if ($errors = $validator->validate()) {
                return ['data' => ["errors" => $errors], 'code' => 422];
            }
            $data["image"] = $filterObject->processImage();
        }
        return ['data' => ["image" => $data["image"]], 'code' => 200];
    }

}
