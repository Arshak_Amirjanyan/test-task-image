<?php

namespace filters;

abstract class AbstractFilter
{
    private string $image;
    private array $rules;
    private array $options;

    /**
     * @param string $image
     * @param array $options
     */
    public function __construct(string $image, array $options, array $rules)
    {
        $this->image = $image;
        $this->options = $options;
        $this->rules = $rules;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     * @return void
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     * @return void
     */
    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public abstract function processImage(): string;

}
