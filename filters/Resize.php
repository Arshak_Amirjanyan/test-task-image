<?php

namespace filters;

class Resize extends AbstractFilter
{
    private array $rules = [
        "length" => ["required", "integer"],
        "width" => ["required", "integer"]
    ];

    public function __construct(string $image, array $options)
    {
        parent::__construct($image, $options, $this->rules);
    }

    public function processImage(): string
    {
        return "resized ". $this->getImage();
    }
}
