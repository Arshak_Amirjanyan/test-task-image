<?php

namespace filters;

class Blur extends AbstractFilter
{
    private array $rules = [
        "depth" => ["required", "integer"]
    ];

    public function __construct(string $image, array $options)
    {
        parent::__construct($image, $options, $this->rules);
    }

    public function processImage(): string
    {
        return "blurred ". $this->getImage();
    }
}
