<?php

define('ROOT', "/" .__DIR__ . "/");

require_once(ROOT . 'vendor/autoload.php');
require_once(ROOT . 'vendor/bootstrap.php');
