<?php

use vendor\components\App;
use vendor\components\Router;


$app = new App();

$router = new Router();
$router->handle();
