<?php
namespace vendor\components;

class App
{
    public static array $configs;

    public static Request $request;

    public static array $routes;


    public function __construct()
    {
        $this->initConfigs();
        $this->initRequest();
        $this->initEnvironment();
        $this->initRoutes();
    }

    /**
     * @return void
     */
    private function initConfigs(): void
    {
        self::$configs = require(ROOT . 'configs/app.php');
    }

    /**
     * @return void
     */
    private function initEnvironment(): void
    {
        if (self::$configs['environment'] === 'local') {
            error_reporting(E_ALL);
        } else {
            error_reporting(0);
        }
    }

    /**
     * @return void
     */
    private function initRoutes(): void
    {
        self::$routes = require_once(ROOT . 'routes/api.php');
    }

    /**
     * @return void
     */
    private function initRequest(): void
    {
        self::$request = new Request;
    }

}
