<?php

namespace vendor\components;


use vendor\components\App;
use vendor\components\Request;

class Router
{
    protected array $routes;

    protected Request $request;

    public function __construct()
    {
        $this->routes = $this->beautifyRoutes();
        $this->request = new Request();
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        if ($action = $this->routes[$this->request->path()] ?? null) {
            $controllerClass = App::$configs['controllers']['base_path'] . $this->parseControllerClass($action);
            $controller = new $controllerClass;
            $response = $controller->{$this->parseControllerMethod($action)}();
            if (isset($response['data']) && isset($response['code'])){
                http_response_code($response['code']);
                echo json_encode($response['data']);
                die();
            }
            echo json_encode($response);
            die();
        }
        http_response_code(404);
        die();
    }

    /**
     * @return array
     */
    public function beautifyRoutes(): array
    {
        $routes = App::$routes;

        foreach ($routes as $route => $controller) {
            unset($routes[$route]);
            $route = ltrim($route, '/');
            $route = rtrim($route, '/');
            $routes[$route] = $controller;
        }

        App::$routes = $routes;
        return App::$routes;
    }

    /**
     * @param string $action
     * @return mixed|string
     */
    public function parseControllerClass(string $action)
    {
        return explode('@', $action)[0];
    }

    /**
     * @param string $action
     * @return mixed|string
     */
    public function parseControllerMethod(string $action)
    {
        return explode('@', $action)[1];
    }
}
