<?php

namespace vendor\components;


class Request
{
    /**
     * @param string $attribute
     * @return string
     */
    public function get(string $attribute): string
    {
        return $_GET[$attribute] ?? '';
    }

    /**
     * @param string $attribute
     * @return string
     */
    public function post(string $attribute): string
    {
        return $_POST[$attribute] ?? '';
    }

    /**
     * @return array
     */
    public function segments(): array
    {
        return $_REQUEST['path'] ? explode('/', $this->path()) : [];
    }

    /**
     * @param int $segment
     * @return string
     */
    public function segment(int $segment): string
    {
        return $this->segments()[$segment];
    }

    /**
     * @return string
     */
    public function path(): string
    {
        return rtrim($_REQUEST['path'] ?? '', '/');
    }
}
