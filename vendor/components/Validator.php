<?php

namespace vendor\components;

class Validator
{
    private array $errors = [];
    private array $data;
    private array $rules;

    public function __construct(array $data, array $rules)
    {
        $this->data = $data;
        $this->rules = $rules;
    }

    /**
     * @param array $rules
     * @return void
     */
    public function setRules(array $rules){
        $this->rules = $rules;
    }

    /**
     * @return array|bool
     */
    public function validate()
    {
        foreach ($this->rules as $field => $rules) {
            foreach ($rules as $rule) {
                $this->$rule($field);
            }
        }
        return $this->errors;
    }

    /**
     * @param $field
     * @return void
     */
    public function required($field): void
    {
        if (!array_key_exists($field, $this->data) || $this->data[$field] === "") {
            $this->errors[] = "Field {$field} is required";
        }
    }

    /**
     * @param $field
     * @return void
     */
    public function string($field): void
    {
        if (array_key_exists($field, $this->data) && !gettype($this->data[$field] === "string")) {
            $this->errors[] = "Field {$field} has to be a string";
        }
    }

    /**
     * @param $field
     * @return void
     */
    public function integer($field): void
    {
        if (array_key_exists($field, $this->data) && gettype($this->data[$field]) !== "integer") {
            $this->errors[] = "Field {$field} has to be an integer";
        }
    }

    /**
     * @return bool
     */
    public function fails(): bool
    {
        if (count($this->errors) > 0) {
            return true;
        }
        return false;
    }
}
